;;; -*- Mode: Lisp -*-

;;; new-append-directories.lisp
;;;
;;; Old code unused in the main file.  Kept here just in case.
;;;
;;; See the Copyright notice in the main file.


(in-package "MAKE")

;;; new-append-directories

(defun new-append-directories (absolute-dir relative-dir)
  
  ;; Version of append-directories for CLtL2-compliant lisps. In particular,
  ;; they must conform to section 23.1.3 "Structured Directories". We are
  ;; willing to fix minor aberations in this function, but not major ones.
  ;; Tested in Allegro CL 4.0 (SPARC), Allegro CL 3.1.12 (DEC 3100),
  ;; CMU CL old and new compilers, Lucid 3.0, Lucid 4.0.
  
  (setf absolute-dir (or absolute-dir "")
	relative-dir (or relative-dir ""))
  
  (let* ((abs-dir (pathname absolute-dir))
	 (rel-dir (pathname relative-dir))
	 
	 (host (pathname-host abs-dir))
	 (device (if (null-string absolute-dir) ; fix for CMU CL old compiler
		     (pathname-device rel-dir)
		     (pathname-device abs-dir)))
	 
	 (abs-directory (directory-to-list (pathname-directory abs-dir)))
	 (abs-keyword (when (keywordp (first abs-directory))
			(pop abs-directory)))
	 
	 ;; Stig (July 2001):
	 ;; Somehow CLISP dies on the next line, but NIL is ok.
	 (abs-name (ignore-errors (file-namestring abs-dir))) ; was pathname-name
	 (rel-directory (directory-to-list (pathname-directory rel-dir)))
	 (rel-keyword (when (keywordp (car rel-directory))
			(pop rel-directory)))
	 
	 ;; rtoy: Why should any Lisp want rel-file?  Shouldn't using
	 ;; rel-name and rel-type work for every Lisp?
         #-(or :MCL :sbcl :clisp :cmu) (rel-file (file-namestring rel-dir))
	 ;; Stig (July 2001);
	 ;; These values seems to help clisp as well
	 #+(or :MCL :sbcl :clisp :cmu) (rel-name (pathname-name rel-dir))
	 #+(or :MCL :sbcl :clisp :cmu) (rel-type (pathname-type rel-dir))
	 (directory nil)
	 )

    ;; TI Common Lisp pathnames can return garbage for file names because
    ;; of bizarreness in the merging of defaults.  The following code makes
    ;; sure that the name is a valid name by comparing it with the
    ;; pathname-name.  It also strips TI specific extensions and handles
    ;; the necessary case conversion.  TI maps upper back into lower case
    ;; for unix files!
    #+TI (if (search (pathname-name abs-dir) abs-name :test #'string-equal)
	     (setf abs-name (string-right-trim "." (string-upcase abs-name)))
	     (setf abs-name nil))
    #+TI (if (search (pathname-name rel-dir) rel-file :test #'string-equal)
	     (setf rel-file (string-right-trim "." (string-upcase rel-file)))
	     (setf rel-file nil))
    
    ;; Allegro v4.0/4.1 parses "/foo" into :directory '(:absolute :root)
    ;; and filename "foo". The namestring of a pathname with
    ;; directory '(:absolute :root "foo") ignores everything after the
    ;; :root.
    
    #+(and allegro-version>= (version>= 4 0))
    (when (eq (car abs-directory) :root) (pop abs-directory))
    #+(and allegro-version>= (version>= 4 0))
    (when (eq (car rel-directory) :root) (pop rel-directory))

    (when (and abs-name (not (null-string abs-name))) ; was abs-name
      (cond ((and (null abs-directory) (null abs-keyword))
	     #-(or :lucid :kcl :akcl TI) (setf abs-keyword :relative)
	     (setf abs-directory
		   (list abs-name)))
	    (t
	     (setf abs-directory
		   (append abs-directory (list abs-name))))))
    
    (when (and (null abs-directory)
	       (or (null abs-keyword)
		   ;; In Lucid, an abs-dir of nil gets a keyword of
		   ;; :relative since (pathname-directory (pathname ""))
		   ;; returns (:relative) instead of nil.
		   #+:lucid (eq abs-keyword :relative))
	       rel-keyword)
      
      ;; The following feature switches seem necessary in CMUCL
      ;; Marco Antoniotti 19990707
      #+(or :sbcl :CMU)
      (if (typep abs-dir 'logical-pathname)
	  (setf abs-keyword :absolute)
	  (setf abs-keyword rel-keyword))
      #-(or :sbcl :CMU)
      (setf abs-keyword rel-keyword))
    
    (setf directory (append abs-directory rel-directory))
    (when abs-keyword (setf directory (cons abs-keyword directory)))
    (namestring
     (make-pathname :host host
		    :device device
                    :directory
                    directory
		    :name
		    #-(or :sbcl :MCL :clisp :cmu) rel-file
		    #+(or :sbcl :MCL :clisp :cmu) rel-name

		    #+(or :sbcl :MCL :clisp :cmu) :type
		    #+(or :sbcl :MCL :clisp :cmu) rel-type
		    )))
  )


(defparameter *append-dirs-tests*
  '("~/foo/" "baz/bar.lisp"
     "~/foo" "baz/bar.lisp"
     "/foo/bar/" "baz/barf.lisp"
     "/foo/bar/" "/baz/barf.lisp"
     "foo/bar/" "baz/barf.lisp"
     "foo/bar" "baz/barf.lisp"
     "foo/bar" "/baz/barf.lisp"
     "foo/bar/" "/baz/barf.lisp"
     "/foo/bar/" nil
     "foo/bar/" nil
     "foo/bar" nil
     "foo" nil
     "foo" ""
     nil "baz/barf.lisp"
     nil "/baz/barf.lisp"
     nil nil))


(defun test-new-append-directories (&optional (test-dirs *append-dirs-tests*))
  (do* ((dir-list test-dirs (cddr dir-list))
	(abs-dir (car dir-list) (car dir-list))
	(rel-dir (cadr dir-list) (cadr dir-list)))
      ((null dir-list) (values))
    (format t "~&ABS: ~S ~18TREL: ~S ~41TResult: ~S"
	    abs-dir rel-dir (new-append-directories abs-dir rel-dir))))


#||
<cl> (test-new-append-directories)

ABS: "~/foo/"     REL: "baz/bar.lisp"    Result: "/usr0/mkant/foo/baz/bar.lisp"
ABS: "~/foo"      REL: "baz/bar.lisp"    Result: "/usr0/mkant/foo/baz/bar.lisp"
ABS: "/foo/bar/"  REL: "baz/barf.lisp"   Result: "/foo/bar/baz/barf.lisp"
ABS: "/foo/bar/"  REL: "/baz/barf.lisp"  Result: "/foo/bar/baz/barf.lisp"
ABS: "foo/bar/"   REL: "baz/barf.lisp"   Result: "foo/bar/baz/barf.lisp"
ABS: "foo/bar"    REL: "baz/barf.lisp"   Result: "foo/bar/baz/barf.lisp"
ABS: "foo/bar"    REL: "/baz/barf.lisp"  Result: "foo/bar/baz/barf.lisp"
ABS: "foo/bar/"   REL: "/baz/barf.lisp"  Result: "foo/bar/baz/barf.lisp"
ABS: "/foo/bar/"  REL: NIL               Result: "/foo/bar/"
ABS: "foo/bar/"   REL: NIL               Result: "foo/bar/"
ABS: "foo/bar"    REL: NIL               Result: "foo/bar/"
ABS: "foo"        REL: NIL               Result: "foo/"
ABS: "foo"        REL: ""                Result: "foo/"
ABS: NIL          REL: "baz/barf.lisp"   Result: "baz/barf.lisp"
ABS: NIL          REL: "/baz/barf.lisp"  Result: "/baz/barf.lisp"
ABS: NIL          REL: NIL               Result: ""

||#



#|| This is incorrect, as it strives to keep strings around, when it
    shouldn't.  MERGE-PATHNAMES already DTRT.
(defun append-logical-pnames (absolute relative)
  (declare (type (or null string pathname) absolute relative))
  (let ((abs (if absolute
		 #-clisp (namestring absolute)
		 #+clisp absolute ;; Stig (July 2001): hack to avoid CLISP from translating the whole string
		 ""))
	(rel (if relative (namestring relative) ""))
	)
    ;; Make sure the absolute directory ends with a semicolon unless
    ;; the pieces are null strings
    (unless (or (null-string abs) (null-string rel)
		(char= (char abs (1- (length abs)))
		       #\;))
      (setq abs (concatenate 'string abs ";")))
    ;; Return the concatenate pathnames
    (concatenate 'string abs rel)))
||#


;;; The code below passes the tests above.  Of course it does not
;;; handle TI Explores CL, but I am afraid it is now extinct in the
;;; wild.

(deftype pathname-designator ()
  '(or string pathname file-stream #| stream |#)
  ;; FILE-STREAM may be too strict, and STREAM is surely too wide.
  )


(defvar *empty-pathname*
  (make-pathname :host nil
		 :device nil
		 :directory nil
		 :name nil
		 :type nil
		 :version nil))

#+first-try
(defun pathname-as-directory (p)
  "Returns a pathname that is a 'directory' pathname.

UN*X and Windows people are used to treat pathnames at the string
(NAMESTRING) level.  This creates problems when people do not realize
that \"foo/bar.baz\" has a 'name' of \"bar\", a 'type' of \"baz\" and
a 'directory' of `(:RELATIVE \"foo\")`, which then breaks expectations
in a variety of cases.

This function takes a pathname P and ensures its transformation into a
'directory' pathname with :NAME and :TYPE set to NIL.  The example
above will return a pathname with :DIRECTORY
`(:RELATIVE \"foo\" \"bar.baz\")`; note how the last directory
component name is constructed, which may wreck havoc on older file
systems like VMS."
  
  (declare (type pathname p))
  (let* ((pd (pathname-directory p))
	 (pn (pathname-name p))
	 (pt (pathname-type p))
	 (nd (cond ((and pn pt)
		    (namestring
		     (make-pathname
		      :name pn
		      :type pt
		      :defaults *empty-pathname*)))
		   (pn pn)
		   (pt
		    (namestring
		     (make-pathname
		      :type pt
		      :defaults *empty-pathname*)))
		   (t ())
		   ))
	 (dir (cond ((and pd nd) (append pd (list nd)))
		    (pd pd)
		    (nd (list :relative nd))
		    (t nil)))
	 )
    (make-pathname
     :directory dir
     :name nil
     :type nil
     :defaults p))
  )



(defun pathname-as-directory (p)
  "Returns a pathname that is a 'directory' pathname.

UN*X and Windows people are used to treat pathnames at the string
(NAMESTRING) level.  This creates problems when people do not realize
that \"foo/bar.baz\" has a 'name' of \"bar\", a 'type' of \"baz\" and
a 'directory' of `(:RELATIVE \"foo\")`, which then breaks expectations
in a variety of cases.

This function takes a pathname P and ensures its transformation into a
'directory' pathname with :NAME and :TYPE set to NIL.  The example
above will return a pathname with :DIRECTORY
`(:RELATIVE \"foo\" \"bar.baz\")`; note how the last directory
component name is constructed, which may wreck havoc on older file
systems like VMS."
  
  (declare (type pathname p))

  (flet ((quote-filename-as-dirname (nd)
	   (declare (type pathname nd))
	   
	   #+(or :unix :linux :windows :mswindows :win32 :msdos :dos)
	   nd
	   #+(or :vms)			; Ok we may not have VMS
					; laying around, and yet...
	   (concatenate 'string
			(pathname-name nd)
			"^."
			(pathname-type nd))
	   #-(or :vms :unix :linux :windows :mswindows :win32 :msdos :dos)
	   nd				; Let'pray.  Maybe warn
	   )
	 )
    (let* ((pd (pathname-directory p))
	   (pn (pathname-name p))
	   (pt (pathname-type p))
	   (nd (cond ((and pn pt)
		      (namestring
		       (quote-filename-as-dirname
			(make-pathname
			 :name pn
			 :type pt
			 :defaults *empty-pathname*))))
		     (pn pn)
		     (pt
		      (namestring
		       (quote-filename-as-dirname
			(make-pathname
			 :type pt
			 :defaults *empty-pathname*))))
		     (t ())
		     ))
	   (dir (cond ((and pd nd) (append pd (list nd)))
		      (pd pd)
		      (nd (list :relative nd))
		      (t nil)))
	   )
      (make-pathname
       :directory dir
       :name nil
       :type nil
       :defaults p))
    ))


(defun relativize-pathname (p)
  (declare (type pathname p))

  (let ((pd (pathname-directory p)))
    (cond ((null pd) (merge-pathnames *empty-pathname* p))
	  ((eq :relative (first pd))
	   (merge-pathnames *empty-pathname* p)) ; Copy the pathname.
	  ((eq :absolute (first pd))
	   (make-pathname :directory (list* :relative (rest pd))
			  :defaults p)
	   )
	  (t (error "MK: cannot handle pathname directory ~S."
		    pd))
	  )
    ))


(defun stitch-names-as-directories (d1 d2)
  ;; Version of NEW-APPEND-DIRECTORIES which should work in 2024 for
  ;; "modern" CLs

  (declare (type (or null pathname-designator) d1 d2))

  (flet ((normalize-name (n)
	   (etypecase n
	     (null (pathname ""))
	     (string (pathname n))
	     (pathname n)
	     (file-stream (pathname n))
	     ))
	 )
    (let* ((nd1 (normalize-name d1))
	   (nd2 (normalize-name d2))
	   (nd1-as-dir (pathname-as-directory nd1))
	   (nd2-as-relative (relativize-pathname nd2))
	   )
      (declare (type pathname nd1 nd2 nd1-as-dir nd2-as-relative))
      (merge-pathnames nd2-as-relative nd1-as-dir)
      )
    ))


(defun test-stitch-names-as-directories (&optional (test-dirs *append-dirs-tests*))
  (do* ((dir-list test-dirs (cddr dir-list))
	(abs-dir (car dir-list) (car dir-list))
	(rel-dir (cadr dir-list) (cadr dir-list)))
       ((null dir-list) (values))
    (format t "~&ABS: ~S ~18TREL: ~S ~41TResult: ~S"
	    abs-dir rel-dir
	    (stitch-names-as-directories abs-dir rel-dir))))


;;; end of file -- new-append-directories.lisp
